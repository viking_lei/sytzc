package com.boyou.sytzc.ui.adapter

import android.content.Context
import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.boyou.sytzc.R
import com.boyou.sytzc.data.entity.NewsInfoEntity

class NewsListAdapter(private val ctx: Context, private val dataSource: MutableList<NewsInfoEntity>?) : BaseAdapter() {

	override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
		val itemData = dataSource?.get(position)
		var itemView: View? = convertView
		val holder: ViewHolder
		if (itemView == null) {
			itemView = View.inflate(ctx, R.layout.item_for_news_info, null)
			holder = ViewHolder()
			holder.txtItem = itemView?.findViewById<TextView>(R.id.txtContent)
			itemView?.tag = holder
		} else {
			holder = itemView.tag as ViewHolder
			itemView = convertView
		}
		holder.txtItem?.setTextColor(Color.parseColor(when (itemData?.color?.length) {
			7 -> itemData.color?.replace("#", "#ff")
			9 -> itemData.color
			else -> "#ff333333"
		}))
		holder.txtItem?.text = itemData?.content
		return itemView!!
	}

	override fun getItem(position: Int): Any? = dataSource?.get(position)

	override fun getItemId(position: Int): Long = position.toLong()

	override fun getCount(): Int = dataSource?.size ?: 0

	private inner class ViewHolder {

		var txtItem: TextView? = null

	}

}
