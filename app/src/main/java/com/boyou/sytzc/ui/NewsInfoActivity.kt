package com.boyou.sytzc.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.boyou.sytzc.R
import com.boyou.sytzc.ui.adapter.NewsListAdapter
import com.boyou.sytzc.data.entity.NewsInfoEntity
import com.boyou.sytzc.databinding.ActivityNewsInfoBinding
import com.boyou.sytzc.utils.data.getArgument
import com.hi.dhl.binding.viewbind

class NewsInfoActivity : AppCompatActivity(), View.OnClickListener {

    private val binding by viewbind<ActivityNewsInfoBinding>()

    private val newsInfos by lazy {
        @Suppress("unchecked_cast") (intent?.getArgument(
            ArrayList::class.java, "newsInfos",
            componentClazz = NewsInfoEntity::class.java
        ) as? ArrayList<NewsInfoEntity>)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_info)
        binding.lvNews.adapter =
            NewsListAdapter(this, newsInfos?.toMutableList() ?: mutableListOf())
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnSubmit -> finish()
        }
    }

}
