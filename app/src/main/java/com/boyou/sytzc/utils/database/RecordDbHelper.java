package com.boyou.sytzc.utils.database;

import android.content.Context;

import com.boyou.sytzc.core.appctx.AppData;
import com.boyou.sytzc.data.entity.RecordInfoEntity;
import com.boyou.sytzc.data.entity.RecordInfoEntity_;
import com.boyou.sytzc.data.entity.UploadInfoEntity;
import com.boyou.sytzc.core.appctx.AppApplication;
import com.yuge.yugecse.util.time.TimeUtil;

import java.util.Calendar;
import java.util.List;

import io.objectbox.Box;

/**
 * Created by bj on 2015/9/16.
 * 数据库辅助类
 */
public class RecordDbHelper {

    private static RecordDbHelper dbHelper;

    public static RecordDbHelper getInstance(Context context) {
        if (dbHelper == null) {
            synchronized (RecordDbHelper.class) {
                dbHelper = new RecordDbHelper(context);
            }
        }
        return dbHelper;
    }

    private Box<RecordInfoEntity> box;

    private RecordDbHelper(Context context) {
        box = ((AppApplication) context.getApplicationContext()).getBoxStore().boxFor(RecordInfoEntity.class);
    }

    /**
     * 获取未上传的数据对象
     **/
    public RecordInfoEntity getNoUploadRecordInfo() {
        return box.query()
                .equal(RecordInfoEntity_.isUpload, 0)
                .orderDesc(RecordInfoEntity_.time)
                .build()
                .findFirst();
    }

    /**
     * 添加一条记录
     **/
    public long addRecord(RecordInfoEntity info) {
        return box.put(info);
    }

    /**
     * 改变记录状态
     **/
    public long changeRecordState(RecordInfoEntity info) {
        return box.put(info);
    }

    /**
     * 改变记录上传信息错误
     **/
    public long changeRecordUploadInfo(RecordInfoEntity info) {
        return box.put(info);
    }

    /**
     * 根据ID查询记录数据
     **/
    public RecordInfoEntity getRecordInfo(int id) {
        return box.get(id);
    }

    /**
     * 五天内是否有数据上传
     **/
    public boolean isUploadedIn5Days(String deviceNo) {
        Calendar calendar = Calendar.getInstance();
        long curTime = calendar.getTimeInMillis();
        calendar.add(Calendar.DATE, -5);
        long beforeTime = calendar.getTimeInMillis();
        List<RecordInfoEntity> dataList = box.query()
                .equal(RecordInfoEntity_.deviceNo, deviceNo)
                .and()
                .between(RecordInfoEntity_.time, beforeTime, curTime)
                .build()
                .find();
        if (dataList.size() > 0) {
            for (RecordInfoEntity item : dataList) {
                if (item.isUpload == 1) return true;
            }
        }
        return false;
    }

    /**
     * 获取所有的记录列表
     **/
    public List<RecordInfoEntity> getAllRecordInfos(int pageIndex) {
        return box.query()
                .orderDesc(RecordInfoEntity_.time)
                .build()
                .find(pageIndex * AppData.PAGE_SIZE, AppData.PAGE_SIZE);
    }

    /**
     * 查询某一类型所有的记录列表
     **/
    public List<RecordInfoEntity> getAllRecordInfos(int type, int pageIndex) {
        return box.query()
                .equal(RecordInfoEntity_.type, type)
                .orderDesc(RecordInfoEntity_.time)
                .build()
                .find(pageIndex * AppData.PAGE_SIZE, AppData.PAGE_SIZE);
    }

    /**
     * 获取今天的所有记录列表
     **/
    public List<RecordInfoEntity> getTodayAllRecordInfos(int pageIndex) {
        return box.query()
                .between(RecordInfoEntity_.time, TimeUtil.getTodayMorningMillins(), TimeUtil.getTodayDeepNightMillins())
                .orderDesc(RecordInfoEntity_.time)
                .build()
                .find(pageIndex * AppData.PAGE_SIZE, AppData.PAGE_SIZE);
    }

    /**
     * 获取今天的某一类型的所有记录列表
     **/
    public List<RecordInfoEntity> getTodayAllRecordInfos(int type, int pageIndex) {
        return box.query()
                .equal(RecordInfoEntity_.type, type)
                .and()
                .between(RecordInfoEntity_.time, TimeUtil.getTodayMorningMillins(), TimeUtil.getTodayDeepNightMillins())
                .orderDesc(RecordInfoEntity_.time)
                .build()
                .find(pageIndex * AppData.PAGE_SIZE, AppData.PAGE_SIZE);
    }

    /**
     * 获取上传的信息
     **/
    public UploadInfoEntity getTodayUploadInfo(int type) {
        UploadInfoEntity info = new UploadInfoEntity();
        List<RecordInfoEntity> all = box.query()
                .equal(RecordInfoEntity_.type, type)
                .and()
                .between(RecordInfoEntity_.time, TimeUtil.getTodayMorningMillins(), TimeUtil.getTodayDeepNightMillins())
                .orderDesc(RecordInfoEntity_.time)
                .build()
                .find();
        for (RecordInfoEntity item : all) {
            if (item.isUpload == 1)
                info.hadUpload++;
        }
        info.total = all.size();
        info.waitUpload = info.total - info.hadUpload;//获取已上传的数据量
        return info;
    }

    /**
     * 获取最后一次添加的记录的图片
     **/
    public String getLastRecordImage(int type) {
        RecordInfoEntity ret = box.query()
                .equal(RecordInfoEntity_.type, type)
                .orderDesc(RecordInfoEntity_.time)
                .build()
                .findFirst();
        return ret != null ? ret.picture : "";
    }


    /**
     * 根据ID删除一条数据
     **/
    public void deleteRecordById(long id) {
        box.remove(id);
    }

    /**
     * 删除一条数据
     **/
    public void deleteRecord(RecordInfoEntity infoEntity) {
        box.remove(infoEntity);
    }

}