@file: JvmName("TimeUtils")

package com.boyou.sytzc.utils.common

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

@SuppressLint("SimpleDateFormat")
fun Long.toDateString(pattern: String) = SimpleDateFormat(pattern).format(Date(this))