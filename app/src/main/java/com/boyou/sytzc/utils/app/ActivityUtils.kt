package com.boyou.sytzc.utils.app

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.os.Build
import android.view.View
import android.view.WindowManager
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.annotation.RequiresApi
import androidx.core.view.*

/**
 * 设置状态栏和导航栏的颜色和状态栏文本模式
 * @param statusBarColor 状态栏颜色, 如果该值为完全透明系颜色，那么标识DecorView是全屏显示的
 * @param isStatusBarLightMode 是否是黑色文字状态栏，默认：TRUE
 * @param navigationBarColor 底部导航栏颜色，默认：黑色 [Color.BLACK]
 * @param isNavigationBarLightMode 是否是黑色按钮导航栏，默认：FALSE
 */
@Suppress("DEPRECATION")
fun Activity.configSystemBarStyle(
    @ColorInt statusBarColor: Int,
    isStatusBarLightMode: Boolean = true,
    @ColorInt navigationBarColor: Int = Color.BLACK,
    isNavigationBarLightMode: Boolean = false
) {
    val rootView = window.decorView
    window.statusBarColor = statusBarColor
    window.navigationBarColor = navigationBarColor
    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
    if (Build.VERSION.SDK_INT in
        arrayOf(Build.VERSION_CODES.LOLLIPOP, Build.VERSION_CODES.LOLLIPOP_MR1)
    ) {
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
    }
    if (statusBarColor == Color.WHITE || statusBarColor == Color.TRANSPARENT) {
        when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.R -> {
                WindowCompat.setDecorFitsSystemWindows(window, false)
                rootView.setOnApplyWindowInsetsListener { view, insets ->
                    val navigationBars = insets.getInsets(WindowInsetsCompat.Type.navigationBars())
                    view.findViewById<View>(android.R.id.content)
                        .updatePadding(bottom = navigationBars.bottom)
                    val insetsCompat = WindowInsetsCompat.toWindowInsetsCompat(insets)
                    ViewCompat.onApplyWindowInsets(view, insetsCompat).toWindowInsets()
                }
            }
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ->
                changeStatusBarMode(isStatusBarLightMode)
            else -> window.setFlags(
                WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
            )
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            rootView.systemUiVisibility = rootView.systemUiVisibility or
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
        }
    }
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O && Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
        rootView.systemUiVisibility =
            rootView.systemUiVisibility or View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR
        if (!isNavigationBarLightMode) {
            rootView.systemUiVisibility =
                rootView.systemUiVisibility xor View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR
        }
    } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
        with(WindowInsetsControllerCompat(window, rootView)) {
            isAppearanceLightNavigationBars =
                if (navigationBarColor == Color.TRANSPARENT) true else isNavigationBarLightMode
            show(WindowInsetsCompat.Type.systemBars())
        }
    }
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R && navigationBarColor == Color.TRANSPARENT) {
        rootView.systemUiVisibility =
            rootView.systemUiVisibility or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
    }
}

/**
 * 设置状态栏和导航栏的颜色和状态栏文本模式
 * @param statusBarColorId 状态栏颜色ID, 如果该值为完全透明系颜色ID [android.R.color.transparent]，那么标识DecorView是全屏显示的
 * @param isStatusBarLightMode 是否是黑色文字状态栏，默认：TRUE
 * @param navigationBarColorId 底部导航栏颜色ID，默认：黑色 [android.R.color.black]
 * @param isNavigationBarLightMode 是否是黑色按钮导航栏，默认：FALSE
 */
@Suppress("DEPRECATION")
fun Activity.configSystemBarStyleWithResources(
    @ColorRes statusBarColorId: Int,
    isStatusBarLightMode: Boolean = true,
    @ColorRes navigationBarColorId: Int = android.R.color.black,
    isNavigationBarLightMode: Boolean = false
) = configSystemBarStyle(
    getColorById(statusBarColorId),
    isStatusBarLightMode,
    getColorById(navigationBarColorId),
    isNavigationBarLightMode
)

/** 修改状态栏上文字图标的颜色 **/
@Suppress("DEPRECATION")
@RequiresApi(Build.VERSION_CODES.M)
fun Activity.changeStatusBarMode(isLightMode: Boolean) = with(window.decorView) {
    systemUiVisibility = systemUiVisibility or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
    if (!isLightMode) {
        systemUiVisibility = systemUiVisibility xor View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
    }
}

/** 根据颜色ID获取颜色 **/
private val Context.getColorById: (Int) -> Int
    get() = { colorId ->
        when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ->
                getColor(colorId)
            else -> resources.getColor(colorId)
        }
    }