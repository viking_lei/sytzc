package com.boyou.sytzc.utils.app

import com.boyou.sytzc.core.appctx.AppData
import com.boyou.sytzc.data.constant.ConfigConst
import com.boyou.sytzc.core.appctx.AppApplication
import com.yuge.yugecse.util.generic.ConfigUtil

class AppConfig private constructor() {

    companion object {

        private var instance: AppConfig? = null

        @JvmStatic
        fun singleton(): AppConfig {
            if (instance == null) synchronized(AppConfig::class.java) {
                if (instance == null) instance = AppConfig()
            }
            return instance!!
        }

    }

    private val configUtil by lazy {
        ConfigUtil(AppApplication.instance, ConfigConst.ConfigName)
    }

    fun contains(key: String) = configUtil.contains(key)

    /** 获取或设置应用是否第一次运行 **/
    var appFirstRun: Boolean
        get() = (configUtil[ConfigConst.TAG_APP_FIRST_RUN, "1"].toString().toIntOrNull()
            ?: 1) == 1
        set(value) = configUtil.put(ConfigConst.TAG_APP_FIRST_RUN, if (value) "1" else "0")

    /** 设置或者获取服务器地址 **/
    var serverUrl: String
        get() = configUtil[ConfigConst.TAG_HTTP_INTERFACE_DOMAIN, AppData.DEFAULT_SERVER_URL].toString()
        set(value) = configUtil.put(ConfigConst.TAG_HTTP_INTERFACE_DOMAIN, value)

    /** 设置或设置wifi上传是否可用 **/
    var isWifiUpload: Boolean
        get() = configUtil[ConfigConst.TAG_WIFI_UPLOAD, false].toString()
            .toBooleanStrictOrNull() ?: false
        set(value) = configUtil.put(ConfigConst.TAG_WIFI_UPLOAD, value)

    /** 获取或设置手动拍照间隔时间 **/
    var handleTimeSpan: Int
        get() = configUtil[ConfigConst.TAG_HANDLE_TIME_SPAN, AppData.DefaultHandleTakeCameraTime].toString()
            .toIntOrNull() ?: AppData.DefaultHandleTakeCameraTime
        set(value) = configUtil.put(ConfigConst.TAG_HANDLE_TIME_SPAN, value)

    /** 获取或设置自动拍照时间间隔 **/
    var autoTimeSpan: Int
        get() = configUtil[ConfigConst.TAG_AUTO_TIME_SPAN, AppData.DefaultAutoTakeCameraTime]
            .toString().toIntOrNull() ?: AppData.DefaultAutoTakeCameraTime
        set(value) = configUtil.put(ConfigConst.TAG_AUTO_TIME_SPAN, value)

    /** 定位坐标数据搜集方式 **/
    var locationMode: Int
        get() = configUtil[ConfigConst.TAG_LOCATION_MODE, 1].toString().toIntOrNull() ?: 1
        set(value) = configUtil.put(ConfigConst.TAG_LOCATION_MODE, value)

    /** 获取或设置司机编号数据 **/
    var driverNoData: String
        get() = configUtil[ConfigConst.TAG_DRIVER_NO_DATA, ""]
        set(value) = configUtil.put(ConfigConst.TAG_DRIVER_NO_DATA, value)

    /** 判断是否有GeoSpacingTime这个字段 **/
    val hasGeoSpacingTime: Boolean
        get() = when (locationMode) {
            0 -> contains(ConfigConst.TAG_LOCATION_SPAN_TIME)
            else -> contains(ConfigConst.TAG_LOCATION_GPS_SPAN_TIME)
        }

    /** GEO间隔时间 **/
    var geoSpacingTime: Int
        get() = when (locationMode) {
            0 -> configUtil[ConfigConst.TAG_LOCATION_SPAN_TIME, AppData.GeoSpacingTime]
            else -> configUtil[ConfigConst.TAG_LOCATION_GPS_SPAN_TIME, AppData.GpsSpacingTime]
        }
        set(value) {
            when (locationMode) {
                0 -> configUtil.put(ConfigConst.TAG_LOCATION_SPAN_TIME, value)
                else -> configUtil.put(ConfigConst.TAG_LOCATION_GPS_SPAN_TIME, value)
            }
        }

    /** 获取或设置是否删除图片 **/
    var allowDeleteImages: Boolean
        get() = configUtil[ConfigConst.TAG_IMAGE_DELETE_ALLOW, false]
        set(value) = configUtil.put(ConfigConst.TAG_IMAGE_DELETE_ALLOW, value)

}