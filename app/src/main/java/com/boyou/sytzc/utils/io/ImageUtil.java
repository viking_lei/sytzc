package com.boyou.sytzc.utils.io;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * Created by Cosecant on 2015/9/25.
 */
public class ImageUtil {

    public static byte[] finalCompress(byte[] data,int width,int height){
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(data,0,data.length,options);
        options.inJustDecodeBounds = false;
        int w = options.outWidth, h = options.outHeight;
        float sw = width,sh = height;
        int scale = 1;//缩放级别，1标识不缩放
        if(w > h && w > sw) scale = (int)(w/sw);
        else if(w < h && h > sh) scale = (int)(h/sh);
        if(scale < 0) scale = 1;
        options.inSampleSize = scale;
        Bitmap bitmap = BitmapFactory.decodeByteArray(data,0,data.length,options);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        int optionQuality = 90;//默认降至90的质量
        bitmap.compress(Bitmap.CompressFormat.JPEG,optionQuality,bos);
        while(bos.size() / 1024 > 80){
            bos.reset();
            bitmap.compress(Bitmap.CompressFormat.JPEG, optionQuality, bos);
            optionQuality -= 10;
        }
        return bos.toByteArray();
    }

    public static byte[] finalCompress(String filename,int width,int height){
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filename, options);
        options.inJustDecodeBounds = false;
        int w = options.outWidth, h = options.outHeight;
        float sw = width,sh = height;
        int scale = 1;//缩放级别，1标识不缩放
        if(w > h && w > sw) scale = (int)(w/sw);
        else if(w < h && h > sh) scale = (int)(h/sh);
        if(scale < 0) scale = 1;
        options.inSampleSize = scale;
        Bitmap bitmap = BitmapFactory.decodeFile(filename,options);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        int optionQuality = 90;//默认降至90的质量
        bitmap.compress(Bitmap.CompressFormat.JPEG,optionQuality,bos);
        while(bos.size() / 1024 > 80){
            bos.reset();
            bitmap.compress(Bitmap.CompressFormat.JPEG, optionQuality, bos);
            optionQuality -= 10;
        }
        return bos.toByteArray();
    }

    public static Bitmap compressImage(Bitmap image) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);//质量压缩方法，这里100表示不压缩，把压缩后的数据存放到baos中
        int options = 100;
        while ( baos.toByteArray().length / 1024>80) {  //循环判断如果压缩后图片是否大于100kb,大于继续压缩
            baos.reset();//重置baos即清空baos
            image.compress(Bitmap.CompressFormat.JPEG, options, baos);//这里压缩options%，把压缩后的数据存放到baos中
            options -= 10;//每次都减少10
        }
        ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());//把压缩后的数据baos存放到ByteArrayInputStream中
        Bitmap bitmap = BitmapFactory.decodeStream(isBm, null, null);//把ByteArrayInputStream数据生成图片
        return bitmap;
    }

}
