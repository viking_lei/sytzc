package com.boyou.sytzc.utils.io

import android.content.Context
import android.net.ConnectivityManager

fun Context.isNetworkAvailable() = (getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).run {
    @Suppress("DEPRECATION")
    activeNetworkInfo?.isAvailable ?: false
}