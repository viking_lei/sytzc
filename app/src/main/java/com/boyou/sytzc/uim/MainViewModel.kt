package com.boyou.sytzc.uim

import androidx.lifecycle.ViewModel
import com.boyou.sytzc.api.ApiService
import com.boyou.sytzc.api.core.HttpApiManager
import com.boyou.sytzc.api.core.handleResult
import com.boyou.sytzc.data.entity.ResponseGetDriverNoInfoEntity
import com.boyou.sytzc.core.appctx.AppApplication
import com.boyou.sytzc.utils.app.AppConfig

/** MainActivity对应的ViewModel **/
class MainViewModel : ViewModel() {

    /** 获取司机编号等信息 **/
    fun getDriverNoInfo(
        onCallback: (Boolean, String, ResponseGetDriverNoInfoEntity?) -> Unit
    ) = HttpApiManager.getApiService(ApiService::class.java)
        .getDriverNo(
            "${AppConfig.singleton().serverUrl}/api.php?h=apprun/getsjno",
            AppApplication.deviceNoManager.deviceNo
        )
        .handleResult { isSuccessful, _, result ->
            onCallback(
                isSuccessful && result?.status == true, result?.message
                    ?: when (isSuccessful) {
                        true -> "获取司机编号等信息成功"
                        else -> "获取司机编号等信息失败，请重试！"
                    }, result
            )
        }

}