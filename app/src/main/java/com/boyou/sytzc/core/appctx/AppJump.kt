package com.boyou.sytzc.core.appctx

import android.app.Activity
import android.os.Bundle
import com.yuge.yugecse.util.generic.ActivityUtil
import com.boyou.sytzc.ui.TakePhotoActivity

/**
 * Created by Mrper on 16.5.15.
 */
object AppJump {

    /**  运行至拍照页面    **/
    @JvmStatic
    fun go2takeCameraActivity(activity: Activity?, type: Int, description: String?) {
        val data = Bundle()
        data.putInt("type", type)
        data.putString("description", description)
        ActivityUtil.goForward(activity, TakePhotoActivity::class.java, data, false)
    }

}