package com.boyou.sytzc.core.dialog

import android.app.Dialog
import android.graphics.Color
import android.os.Bundle
import com.boyou.sytzc.R
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.view.Window
import android.widget.EditText
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager

/**
 * Created by Mrper on 16.5.14.
 * 密码确认输入框
 */
class PasswordConfirmDialog : DialogFragment() {
    fun interface OnPasswordVerifyListener {
        fun onPasswordVerify()
    }

    var onPasswordVerifyListener: OnPasswordVerifyListener? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val contentView = View.inflate(context, R.layout.dialog_password_confirm, null)
        val dialog = Dialog(requireContext())
        val window = dialog.window
        window!!.requestFeature(Window.FEATURE_NO_TITLE)
        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(contentView)
        val etVerifyCode = contentView.findViewById<View>(R.id.etVerfyCode) as EditText
        val onClickListener = View.OnClickListener { v: View ->
            if (v.id == R.id.btnVerfy) {
                if (etVerifyCode.text.toString().trim { it <= ' ' } != "syt888666") {
                    etVerifyCode.error = "您输入的口令有误，请重新输入"
                    return@OnClickListener
                }
                onPasswordVerifyListener?.onPasswordVerify()
            }
            dialog.dismiss()
        }
        contentView.findViewById<View>(R.id.btnCancel).setOnClickListener(onClickListener)
        contentView.findViewById<View>(R.id.btnVerfy).setOnClickListener(onClickListener)
        return dialog
    }

    /**   显示对话框   */
    fun show(manager: FragmentManager?) {
        super.show(manager!!, TAG)
    }

    companion object {
        val TAG = PasswordConfirmDialog::class.java.simpleName
    }
}