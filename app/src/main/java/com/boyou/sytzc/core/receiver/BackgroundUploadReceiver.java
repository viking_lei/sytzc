package com.boyou.sytzc.core.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.boyou.sytzc.data.event.UploadNotifyEvent;
import com.boyou.sytzc.core.service.UploadService;

import org.simple.eventbus.EventBus;

/**
 * Created by Cosecant on 2015/9/19.
 * 后台上传通知任务
 */
public class BackgroundUploadReceiver extends BroadcastReceiver {

    public BackgroundUploadReceiver() {
        super();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        context.startService(new Intent(context, UploadService.class));//启动后台上传服务
        String action = intent.getAction();
        switch (action){
            case Intent.ACTION_SCREEN_ON://通知开启上传
                EventBus.getDefault().post(new UploadNotifyEvent(false));
                break;
            case Intent.ACTION_SCREEN_OFF://通知关闭上传
                EventBus.getDefault().post(new UploadNotifyEvent(true));
                break;
        }
    }
}
