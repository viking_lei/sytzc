package com.boyou.sytzc.core.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.boyou.sytzc.core.service.UploadService;
/**
 * Created by Cosecant on 2015/9/19.
 * 后台上传通知任务
 */
public class BackgroundThreadReceiver extends BroadcastReceiver {

    public BackgroundThreadReceiver() {
        super();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        switch (action){
            case Intent.ACTION_BOOT_COMPLETED:
            case Intent.ACTION_USER_PRESENT:
                context.startService(new Intent(context, UploadService.class));//启动后台上传服务
                break;
        }
    }
}
