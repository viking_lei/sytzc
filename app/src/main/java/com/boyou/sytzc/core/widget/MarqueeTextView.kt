package com.boyou.sytzc.core.widget

import android.content.Context
import android.text.TextUtils
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView

/** 滚动文字控件 **/
class MarqueeTextView : AppCompatTextView {

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    private fun init() {
        maxLines = 1
        minLines = 1
        ellipsize = TextUtils.TruncateAt.MARQUEE
        isFocusable = true
    }

    override fun isFocused(): Boolean = true

}
