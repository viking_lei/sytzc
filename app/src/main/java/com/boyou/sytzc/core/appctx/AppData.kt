package com.boyou.sytzc.core.appctx

/**
 * Created by bj on 2015/9/16.
 * 常量类
 */
object AppData {

    /**   缓存地址   */
    @JvmField
    var CACHE_PATH = "/mnt/sdcard/syt/"

    /**  获取图片缓存地址  */
    val IMAGE_CACHE_PATH: String
        @JvmStatic get() = "${CACHE_PATH}image/"

    /**
     * 默认服务器地址
     */
    const val DEFAULT_SERVER_URL = "http://syt.goworldchina.com"

    /**
     * 每页数量
     */
    const val PAGE_SIZE = 5

    /**
     * Geo坐标采集时间
     */
    const val GeoSpacingTime = 25

    /**
     * GPS坐标采集时间
     */
    const val GpsSpacingTime = 2

    /**
     * 默认手动拍照时间
     */
    const val DefaultHandleTakeCameraTime = 90

    /**
     * 默认自动拍照时间
     */
    const val DefaultAutoTakeCameraTime = 90
}