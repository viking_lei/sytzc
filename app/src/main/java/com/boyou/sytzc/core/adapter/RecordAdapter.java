package com.boyou.sytzc.core.adapter;

import android.annotation.SuppressLint;
import android.content.Context;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.boyou.sytzc.R;
import com.boyou.sytzc.data.entity.RecordInfoEntity;
import com.boyou.sytzc.utils.app.AppConfig;
import com.squareup.picasso.Picasso;
import com.yuge.yugecse.ext.adapter.BasicAdapter;
import com.yuge.yugecse.util.device.ToastUtil;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by bj on 2015/9/16.
 * 记录数据适配器
 */
public class RecordAdapter extends BasicAdapter<RecordInfoEntity> {

    private static final int id_imgPicture = R.id.listitem_record_imgPicture;
    private static final int id_txtUploadinfo = R.id.listitem_record_txtUploadInfo;
    private static final int id_txtMode = R.id.listitem_record_txtMode;
    private static final int id_txtState = R.id.listitem_record_txtState;
    private static final int id_llOtherType = R.id.llOtherType;
    private static final int id_txtDriverNo = R.id.listitem_record_txtDriverNo;
    private static final int id_txtRockType = R.id.listitem_record_txtRockType;
    private static final int id_txtCarType = R.id.listitem_record_txtCarType;
    private static final int id_txtLoadType = R.id.listitem_record_txtLoadType;
    private static final int id_txtAddress = R.id.listitem_record_txtAddress;
    private static final int id_txtTime = R.id.listitem_record_txtTime;
    private static final int id_imgDelete = R.id.imgDelete;

    public RecordAdapter(@Nullable Context context) {
        super(context, R.layout.listitem_record);
    }

    @Override
    public void initItemView(ViewHolder holder) {
        holder.addChildIds(id_imgPicture, id_txtUploadinfo, id_txtMode,
                id_txtState, id_llOtherType, id_txtDriverNo, id_txtRockType, id_txtCarType,
                id_txtLoadType, id_txtAddress, id_txtTime, id_imgDelete);
    }

    @SuppressLint("SimpleDateFormat")
    @Override
    public void setItemValue(ViewHolder holder, int position, RecordInfoEntity item) {
        final ImageView imgPicture = (ImageView) holder.getChildById(id_imgPicture);
        if (!TextUtils.isEmpty(item.picture)) {
            assert item.picture != null;
            final File imageFile = new File(item.picture);
            if (imageFile.exists()) {
                Picasso.get().load(imageFile)
                        .placeholder(R.mipmap.ic_default_loading)
                        .error(R.mipmap.default_img)
                        .into(imgPicture);
                ((View) imgPicture.getParent()).setVisibility(View.VISIBLE);
            } else {
                ((View) imgPicture.getParent()).setVisibility(View.GONE);
            }
            final ImageView imgDelete = (ImageView) holder.getChildById(id_imgDelete);
            if (!AppConfig.singleton().getAllowDeleteImages()) {
                imgDelete.setVisibility(View.GONE);
            } else {
                imgDelete.setVisibility(View.VISIBLE);
                imgDelete.setOnClickListener(v -> new AlertDialog.Builder(context)
                        .setMessage("是否删除图片？")
                        .setPositiveButton("删除", (dialog, which) -> {
                            if (imageFile.delete()) {
                                notifyDataSetChanged();
                            } else {
                                ToastUtil.showShortToast(context, "图片删除失败", Gravity.CENTER);
                            }
                        })
                        .setNegativeButton("取消", null)
                        .show());
            }
        }
        ((TextView) holder.getChildById(id_txtUploadinfo)).setText(item.uploadInfo);//上传信息展示
        ((TextView) holder.getChildById(id_txtMode)).setText(String.format("模式：%s", item.type == 1 ? "渣车模式" : "计时模式"));
        ((TextView) holder.getChildById(id_txtState)).setText(String.format("状态：%s", item.isUpload == 0 ? "未上传" : "已上传"));
        if (item.type == 1) {//如果是装车模式
            ((TextView) holder.getChildById(id_txtRockType)).setText(String.format("位   置：%s", item.posi == 1 ? "装土点" : "卸土点"));
            ((TextView) holder.getChildById(id_txtDriverNo)).setText(String.format("司机编号：%s", TextUtils.isEmpty(item.sjNo) ? "未知" : item.sjNo));
            ((TextView) holder.getChildById(id_txtCarType)).setText(String.format("挖机编号：%s", TextUtils.isEmpty(item.carNo) ? "未知" : item.carNo));
            ((TextView) holder.getChildById(id_txtLoadType)).setText(String.format("线路编号：%s", TextUtils.isEmpty(item.lineNo) ? "未知" : item.lineNo));
            holder.getChildById(id_llOtherType).setVisibility(View.VISIBLE);
        } else {
            holder.getChildById(id_llOtherType).setVisibility(View.GONE);
        }
        ((TextView) holder.getChildById(id_txtAddress)).setText(String.format("地点：%s", TextUtils.isEmpty(item.address) ? "未知" : item.address));
        ((TextView) holder.getChildById(id_txtTime)).setText(String.format("时间：%s", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(item.time))));

    }


}
