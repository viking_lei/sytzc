package com.boyou.sytzc.api

import com.boyou.sytzc.data.entity.GetNewsListCallback
import com.boyou.sytzc.data.entity.GetServerLocalsCallback
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import retrofit2.http.Url

interface HttpApiService {

	/**
	 * 获取服务器地址
	 * @param deviceNo 设备号
	 * @param type 类型，1-挖机 2-渣车 3-油料  4-物料
	 */
	@FormUrlEncoded
	@POST("http://www.zggcgl.com/api/app/getsev")
	fun getServerLocals(
		@Field("no") deviceNo: String?,
		@Field("cat") type: Int
	): Observable<GetServerLocalsCallback>


	/**
	 * 设备验证,返回描述内容信息
	 * @param requestUrl api.php?h=app/check
	 * @param deviceNo 设备号
	 */
	@FormUrlEncoded
	@POST
	fun validateDeviceNo(
		@Url requestUrl: String,
		@Field("no") deviceNo: String?
	): Observable<GetNewsListCallback>

}