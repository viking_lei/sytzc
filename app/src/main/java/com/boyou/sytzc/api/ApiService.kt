package com.boyou.sytzc.api

import com.boyou.sytzc.data.entity.ResponseGetCodeInfoEntity
import com.boyou.sytzc.data.entity.ResponseGetDriverNoInfoEntity
import com.boyou.sytzc.data.entity.ResponseUploadRecordInfoEntity
import io.reactivex.Observable
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface ApiService {

    /**
     * 获取编码等信息
     *
     * @param baseUrl 基础地址
     * @param deviceNo 设备号
     */
    @FormUrlEncoded
    @POST
    fun getCode(
            @Url baseUrl: String?,
            @Field("no") deviceNo: String?
    ): Observable<ResponseGetCodeInfoEntity>

    /**
     * 获取司机编号信息
     *
     * @param baseUrl 基础地址
     * @param deviceNo 设备号
     */
    @FormUrlEncoded
    @POST
    fun getDriverNo(
            @Url baseUrl: String?,
            @Field("no") deviceNo: String?
    ): Observable<ResponseGetDriverNoInfoEntity>

    /**
     * 上传记录信息
     *
     * @param baseUrl 基础地址
     * @param paramMaps 参数信息
     */
    @Multipart
    @POST
    fun uploadRecord(
            @Url baseUrl: String?,
            @PartMap paramMaps: LinkedHashMap<String, RequestBody>?
    ): Call<ResponseUploadRecordInfoEntity>

}