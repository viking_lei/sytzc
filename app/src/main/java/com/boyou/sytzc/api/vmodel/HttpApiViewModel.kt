package com.boyou.sytzc.api.vmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.boyou.sytzc.api.ApiService
import com.boyou.sytzc.api.HttpApiService
import com.boyou.sytzc.api.core.HttpApiManager
import com.boyou.sytzc.api.core.handleResult
import com.boyou.sytzc.data.entity.NewsInfoEntity
import com.boyou.sytzc.data.entity.ResponseGetCodeInfoEntity
import com.boyou.sytzc.data.entity.ResponseGetDriverNoInfoEntity
import com.boyou.sytzc.data.entity.ServerLocalInfoEntity
import com.boyou.sytzc.core.appctx.AppApplication
import com.boyou.sytzc.utils.app.AppConfig

class HttpApiViewModel(private val app: Application) : AndroidViewModel(app) {

    /**
     * 获取服务器列表数据
     * @param type  类型，1-挖机 2-渣车 3-油料 4-物料
     * @param deviceNo 设备号
     * @param onCallback 回调参数
     */
    fun getServerLocals(
        type: Int,
        deviceNo: String?,
        onCallback: (Boolean, String, List<ServerLocalInfoEntity>?) -> Unit
    ) = HttpApiManager.getApiService(HttpApiService::class.java)
        .getServerLocals(deviceNo, type)
        .handleResult { isSuccessful, _, result ->
            onCallback(
                isSuccessful, result?.message ?: when (isSuccessful) {
                    true -> "获取成功"
                    else -> "获取失败，请重试！"
                }, result?.serverLocals
            )
        }


    /**
     * 设备验证,返回描述内容信息
     * @param deviceNo 设备号
     */
    fun validateDeviceNo(
        deviceNo: String?,
        onCallback: (Boolean, String, List<NewsInfoEntity>?) -> Unit
    ) = HttpApiManager.getApiService(HttpApiService::class.java)
        .validateDeviceNo("${AppConfig.singleton().serverUrl}/api.php?h=app/check", deviceNo)
        .handleResult { isSuccessful, _, result ->
            onCallback(
                isSuccessful, result?.message ?: when (isSuccessful) {
                    true -> "验证设备信息成功"
                    else -> "验证设备信息失败，请重试！"
                }, result?.newsList
            )
        }


    /** 获取编号等信息内容 **/
    fun getCode(
        onCallback: (Boolean, String, ResponseGetCodeInfoEntity?) -> Unit
    ) = HttpApiManager.getApiService(ApiService::class.java)
        .getCode(
            "${AppConfig.singleton().serverUrl}/api.php?h=device/getparam",
            AppApplication.deviceNoManager.deviceNo
        )
        .handleResult { isSuccessful, _, result ->
            onCallback(
                isSuccessful && result?.status == true, result?.message
                    ?: when (isSuccessful) {
                        true -> "获取编号等基础信息成功"
                        else -> "获取编号等基础信息失败，请重试！"
                    }, result
            )
        }

    /** 获取司机编号等信息 **/
    fun getDriverNoInfo(
        onCallback: (Boolean, String, ResponseGetDriverNoInfoEntity?) -> Unit
    ) = HttpApiManager.getApiService(ApiService::class.java)
        .getDriverNo(
            "${AppConfig.singleton().serverUrl}/api.php?h=apprun/getsjno",
            AppApplication.deviceNoManager.deviceNo
        )
        .handleResult { isSuccessful, _, result ->
            onCallback(
                isSuccessful && result?.status == true, result?.message
                    ?: when (isSuccessful) {
                        true -> "获取司机编号等信息成功"
                        else -> "获取司机编号等信息失败，请重试！"
                    }, result
            )
        }


}