package com.boyou.sytzc.data.entity

import com.google.gson.annotations.SerializedName

/**
 * Created by Mrper on 16-3-14.
 */
data class ResponseGetCodeInfoEntity(
        @field: SerializedName("state")
        override var status: Boolean?,
        @field: SerializedName("err")
        override var message: String?,
        var code: Int = 0,
        var car: List<String>? = null,
        var line: List<String>? = null
) : ResponseModel