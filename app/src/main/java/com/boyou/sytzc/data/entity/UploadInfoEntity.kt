package com.boyou.sytzc.data.entity

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by bj on 2015/9/16.
 * 上传信息实体类
 */
class UploadInfoEntity : Parcelable {
    /** 数据总数量  */
    @JvmField
    var total = 0
    /** 已上传数据  */
    @JvmField
    var hadUpload = 0
    /** 等待上传的数据  */
    @JvmField
    var waitUpload = 0

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeInt(total)
        dest.writeInt(hadUpload)
        dest.writeInt(waitUpload)
    }

    constructor() {}
    protected constructor(`in`: Parcel) {
        total = `in`.readInt()
        hadUpload = `in`.readInt()
        waitUpload = `in`.readInt()
    }

    companion object {
        @JvmField val CREATOR: Parcelable.Creator<UploadInfoEntity?> = object : Parcelable.Creator<UploadInfoEntity?> {
            override fun createFromParcel(source: Parcel): UploadInfoEntity? {
                return UploadInfoEntity(source)
            }

            override fun newArray(size: Int): Array<UploadInfoEntity?> {
                return arrayOfNulls(size)
            }
        }
    }
}