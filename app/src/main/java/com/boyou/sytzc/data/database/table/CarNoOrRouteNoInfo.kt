package com.boyou.sytzc.data.database.table

import android.os.Parcelable
import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id
import kotlinx.parcelize.Parcelize

/**
 * 汽车号或路线号信息实体类
 * @param id 数据ID
 * @param cid 数据ID，暂时无效
 * @param name 名称
 * @param type 类型，0-汽车号，1-路线号
 */
@Entity
@Parcelize
data class CarNoOrRouteNoInfo(
    @Id var id: Long? = 0,
    var cid: Long? = null,
    var name: String? = null,
    var type: Int? = null
) : Parcelable