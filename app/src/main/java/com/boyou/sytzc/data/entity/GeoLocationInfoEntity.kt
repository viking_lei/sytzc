package com.boyou.sytzc.data.entity

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.GsonBuilder
import com.google.gson.annotations.Expose
import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id
import io.objectbox.annotation.apihint.Internal
import java.util.*

/**
 * Created by mrper on 17-7-24.
 * 地理坐标实体类
 */
@Entity
class GeoLocationInfoEntity : Parcelable {
    @Id(assignable = true)
    var id: Long = 0
    @Expose
    var longitude: Double? = null
    @Expose
    var latitude: Double? = null
    var addressName: String? = null
    @JvmField
    @Expose
    var createTime: Date? = null
    var isUpload = false

    @Internal
            /** This constructor was generated by ObjectBox and may change any time.  */
    constructor(id: Long, longitude: Double?, latitude: Double?, addressName: String?, createTime: Date?,
                isUpload: Boolean) {
        this.id = id
        this.longitude = longitude
        this.latitude = latitude
        this.addressName = addressName
        this.createTime = createTime
        this.isUpload = isUpload
    }
    constructor() {
    }

    constructor(source: Parcel) {
        id = source.readLong()
        longitude = source.readDouble()
        latitude = source.readDouble()
        addressName = source.readString()
        createTime = source.readValue(Date::class.java.classLoader) as Date
        isUpload = source.readValue(Boolean::class.java.classLoader) as Boolean
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeLong(id)
        dest.writeDouble(longitude!!)
        dest.writeDouble(latitude!!)
        dest.writeString(addressName)
        dest.writeValue(createTime)
        dest.writeValue(isUpload)
    }

    override fun toString(): String {
        return GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").serializeNulls().create().toJson(this)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<GeoLocationInfoEntity?> = object : Parcelable.Creator<GeoLocationInfoEntity?> {
            override fun createFromParcel(source: Parcel): GeoLocationInfoEntity? {
                return GeoLocationInfoEntity(source)
            }

            override fun newArray(size: Int): Array<GeoLocationInfoEntity?> {
                return arrayOfNulls(size)
            }
        }
    }
}