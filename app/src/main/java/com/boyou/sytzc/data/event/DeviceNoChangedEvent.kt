package com.boyou.sytzc.data.event

/**
 * Created by mrper on 17-7-27.
 * 设备号变化事件
 */
data class DeviceNoChangedEvent(
        @JvmField var deviceNo: String?
)