package com.boyou.sytzc.data.entity

import android.os.Parcel
import android.os.Parcelable
import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id
import io.objectbox.annotation.NameInDb

/**
 * Created by bj on 2015/9/16.
 * 记录信息实体类
 */
@Entity
class RecordInfoEntity : Parcelable {
    /**
     * 数据ID
     */
    @Id
    var id: Long = 0
    /**
     * 设备号
     */
    @JvmField
    @NameInDb("deviceNo")
    var deviceNo: String? = null
    /**
     * 类型数据,0 - 装车模式，1 - 计时模式
     */
    @JvmField
    @NameInDb("type")
    var type = 0
    /**
     * 位置：1-装货点   2-卸货点
     */
    @JvmField
    @NameInDb("posi")
    var posi = 0
    /**
     * 挖机编号
     */
    @JvmField
    @NameInDb("carNo")
    var carNo: String? = null
    /**
     * 线路编号
     */
    @JvmField
    @NameInDb("lineNo")
    var lineNo: String? = null
    /**
     * 司机编号
     */
    @JvmField
    @NameInDb("sjno")
    var sjNo: String? = null
    /**
     * 数据创建时间
     */
    @JvmField
    @NameInDb("time")
    var time: Long = 0
    /**
     * 图片本地地址
     */
    @JvmField
    @NameInDb("picture")
    var picture: String? = null
    /**
     * 地理位置
     */
    @JvmField
    @NameInDb("address")
    var address: String? = null
    /**
     * 位置坐标x
     */
    @JvmField
    @NameInDb("loc_x")
    var locx = 0.0
    /**
     * 位置坐标y
     */
    @JvmField
    @NameInDb("loc_y")
    var locy = 0.0
    /**
     * 是否已经上传,0 - 为上传，1 - 已上传
     */
    @JvmField
    @NameInDb("is_upload")
    var isUpload = 0
    /**
     * 上传信息
     */
    @JvmField
    @NameInDb("upload_info")
    var uploadInfo = "等待上传"
    /**
     * Geo的坐标信息
     */
    @JvmField
    @NameInDb("geoPacket")
    var packet: String? = null

    constructor() {}
    protected constructor(`in`: Parcel) {
        id = `in`.readLong()
        deviceNo = `in`.readString()
        type = `in`.readInt()
        posi = `in`.readInt()
        carNo = `in`.readString()
        lineNo = `in`.readString()
        sjNo = `in`.readString()
        time = `in`.readLong()
        picture = `in`.readString()
        address = `in`.readString()
        locx = `in`.readDouble()
        locy = `in`.readDouble()
        isUpload = `in`.readInt()
        uploadInfo = `in`.readString() ?: ""
        packet = `in`.readString()
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeLong(id)
        dest.writeString(deviceNo)
        dest.writeInt(type)
        dest.writeInt(posi)
        dest.writeString(carNo)
        dest.writeString(lineNo)
        dest.writeString(sjNo)
        dest.writeLong(time)
        dest.writeString(picture)
        dest.writeString(address)
        dest.writeDouble(locx)
        dest.writeDouble(locy)
        dest.writeInt(isUpload)
        dest.writeString(uploadInfo)
        dest.writeString(packet)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<RecordInfoEntity?> = object : Parcelable.Creator<RecordInfoEntity?> {
            override fun createFromParcel(`in`: Parcel): RecordInfoEntity? {
                return RecordInfoEntity(`in`)
            }

            override fun newArray(size: Int): Array<RecordInfoEntity?> {
                return arrayOfNulls(size)
            }
        }
    }
}