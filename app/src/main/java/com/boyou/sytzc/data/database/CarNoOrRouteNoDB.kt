package com.boyou.sytzc.data.database

import com.boyou.sytzc.data.database.table.CarNoOrRouteNoInfo
import com.boyou.sytzc.data.database.table.CarNoOrRouteNoInfo_
import com.boyou.sytzc.core.appctx.AppApplication
import io.objectbox.kotlin.query

/** 记录汽车编号数据库 **/
class CarNoOrRouteNoDB private constructor() {

    companion object {

        private var instance: CarNoOrRouteNoDB? = null

        @JvmStatic
        fun singleton(): CarNoOrRouteNoDB {
            if (instance == null) synchronized(CarNoOrRouteNoDB::class.java) {
                if (instance == null) instance = CarNoOrRouteNoDB()
            }
            return instance!!
        }

    }

    private val box by lazy { AppApplication.instance.boxStore.boxFor(CarNoOrRouteNoInfo::class.java) }

    fun put(info: CarNoOrRouteNoInfo) = box.put(info)

    fun put(infoList: Collection<CarNoOrRouteNoInfo>) = box.put(infoList)

    fun findById(id: Long) = box.query { equal(CarNoOrRouteNoInfo_.id, id) }.findFirst()

    fun findByType(type: Int) = box.query { equal(CarNoOrRouteNoInfo_.type, type.toLong()) }.find()

    fun remove(id: Long) = box.remove(id)

    fun remove(info: CarNoOrRouteNoInfo) = box.remove(info)

    fun remove(infoList: Collection<CarNoOrRouteNoInfo>) = box.remove(infoList)

    fun clear(type: Int) = box.remove(findByType(type))

    fun clear() = box.removeAll()

}