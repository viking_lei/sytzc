package com.boyou.sytzc.data.event;

/**
 * Created by Cosecant on 2015/9/19.
 * 上传信息通知事件
 */
public class UploadNotifyEvent {

    public boolean isUpload;

    public UploadNotifyEvent(boolean isUpload){
        this.isUpload = isUpload;
    }

}
