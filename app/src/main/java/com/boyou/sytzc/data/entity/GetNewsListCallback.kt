package com.boyou.sytzc.data.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class GetNewsListCallback(
		@field: SerializedName("status")
        override var status: Boolean?,
		@field: SerializedName("err")
        override var message: String?,
        @field: SerializedName("news")
        var newsList: List<NewsInfoEntity>? = null
) : ResponseModel, Parcelable

@Parcelize
data class NewsInfoEntity(
        @field: SerializedName("color")
        var color: String? = null,
        @field:SerializedName("text")
        var content: String? = null
) : Parcelable