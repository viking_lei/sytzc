package com.boyou.sytzc.data.entity

interface ResponseModel {
    var status: Boolean?
    var message: String?
}
