package com.boyou.sytzc.data.constant

/**
 * Created by Cosecant on 2015/9/21.
 */
object ConfigConst {

    /** 配置文件名称 **/
    const val ConfigName = "config.ini"

    /** 应用是否是第一次运行 **/
    const val TAG_APP_FIRST_RUN = "app_first_run"

    /** wifi上传的标识 **/
    const val TAG_WIFI_UPLOAD = "wifi_upload"

    /** 定位获取间隔时间 **/
    const val TAG_LOCATION_SPAN_TIME = "location_span_time"

    /** GPS定位获取时间间隔 **/
    const val TAG_LOCATION_GPS_SPAN_TIME = "location_gps_span_time"

    /** 手动拍照的时间间隔标识 **/
    const val TAG_HANDLE_TIME_SPAN = "handle_time_span"

    /** 自动拍照的时间间隔标识 **/
    const val TAG_AUTO_TIME_SPAN = "auto_time_span"

    /** 定位收集模式 **/
    const val TAG_LOCATION_MODE = "location_mode"

    /** 数据上传地址的URL接口 **/
    const val TAG_HTTP_INTERFACE_DOMAIN = "http_interface_domain"

    /** 司机编号数据 **/
    const val TAG_DRIVER_NO_DATA = "tag_driver_no_data"

    /** 记录图片是否允许删除 **/
    const val TAG_IMAGE_DELETE_ALLOW = "image_delete_allow"

    //    /** 车辆数据  **/
    //    public static final String CARS_DATA = "cars_data";
    //
    //    /** 路线数据   **/
    //    public static final String LINES_DATA = "lines_data";
    //    /** 地址搜索时间间隔 **/
    //    public static final String ADDRESS_SCAN_TIME = "address_scan_time";
}