package com.boyou.sytzc.data.entity

import com.google.gson.annotations.SerializedName

data class GetServerLocalsCallback(
		@field: SerializedName("status")
        override var status: Boolean?,
		@field: SerializedName("err")
		override var message: String?,
        @field: SerializedName("sev")
        var serverLocals: List<ServerLocalInfoEntity>? = null
) : ResponseModel

data class ServerLocalInfoEntity(
        @field: SerializedName("name")
        var name: String? = null,
        @field:SerializedName("url")
        var url: String? = null,
        @field: SerializedName("wjpz")
        var wjpz: Int? = null,
        @field: SerializedName("wjjs")
        var wjjs: Int? = null,
        @field: SerializedName("zczb")
        var zczb: Int? = null,
        @field: SerializedName("zcpz")
        var zcpz: Int? = null
) {

    override fun toString(): String = name ?: ""

}