package com.boyou.sytzc.data.entity

import com.google.gson.annotations.SerializedName

/**
 * Created by Mrper on 16-3-14.
 * 获取司机编号信息等内容
 */
data class ResponseGetDriverNoInfoEntity(
        @field: SerializedName("state")
        override var status: Boolean?,
        @field: SerializedName("err")
        override var message: String?,
        var code: Int = 0,
        var sj: List<String>? = null
) : ResponseModel