package com.yuge.yugecse.ext.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import com.yuge.yugecse.R;
import com.yuge.yugecse.util.device.DensityUtil;

/**
 * Created by Administrator on 2015/4/24.
 */
public class BadageView extends View {

    private Context context;
    private Resources resources;
    private Paint paint = new Paint();
    private float width = 0f,height = 0f;
    private int bgColor = Color.RED,txtColor = Color.WHITE,borderColor = Color.TRANSPARENT;
    private boolean hasBorder = false;
    private float borderWidth = 0;
    private float textSize = 15f;
    private int num = 0;//需要显示的数字

    public BadageView(Context context) {
        super(context);
        init(context,null);
    }

    public BadageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context,attrs);
    }

    public BadageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context,AttributeSet attrs){
        if(attrs!=null) {//如果有用户定义的属性
            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.BadageView);
            borderWidth = typedArray.getDimension(R.styleable.BadageView_borderWidth1, 3f);
            bgColor = typedArray.getColor(R.styleable.BadageView_circleBackgroundColor, Color.RED);
            txtColor = typedArray.getColor(R.styleable.BadageView_numColor, Color.WHITE);
            borderColor = typedArray.getColor(R.styleable.BadageView_borderColor, Color.GRAY);
            hasBorder = typedArray.getBoolean(R.styleable.BadageView_hasBorder, false);
            textSize = typedArray.getDimension(R.styleable.BadageView_numSize, 30f);
            num = typedArray.getInteger(R.styleable.BadageView_num, 0);
            typedArray.recycle();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //获取尺寸
        width = getMeasuredWidth();
        height = getMeasuredHeight();
        //绘制图像
        if(num > 0) {//数字大于0才显示
            paint.reset();
            paint.setAntiAlias(true);
            //绘制圆
            paint.setColor(bgColor);
            if(hasBorder){//如果右边框，则绘制边框
                paint.setStyle(Paint.Style.FILL_AND_STROKE);
                paint.setStrokeWidth(borderWidth);
                paint.setColor(borderColor);
            }
            canvas.drawOval(new RectF(0, 0, width, height), paint);
            //绘制文字
            paint.setStyle(Paint.Style.FILL);
            paint.setColor(txtColor);
            paint.setTextSize((num < 100 ? textSize : textSize*2/3));
            String text = num > 99?"99+":String.valueOf(num);
            canvas.drawText(text, (width - getFontWidth(paint, text)) / 2,
                    (height - getFontHeight(paint, text)) / 2, paint);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int ws = MeasureSpec.getMode(widthMeasureSpec);
        int hs = MeasureSpec.getMode(heightMeasureSpec);
        int rws = 0,rhs = 0;
        if(ws == MeasureSpec.UNSPECIFIED && hs ==MeasureSpec.UNSPECIFIED){
            rws = rhs = DensityUtil.dip2px(getContext(), 15f);
        }else {
            rws = widthMeasureSpec;
            rhs = heightMeasureSpec;
        }
        super.onMeasure(rws, rhs);
    }

    /** 设置需要显示的数字 **/
    public void setNum(int num){
        this.num = num;
        this.setVisibility(this.num > 0 ? View.VISIBLE:View.GONE);
        invalidate();
    }

    /** 获取需要显示的数字 **/
    public int getNum(){
        return num;
    }

    private float getFontWidth(Paint paint, String text){
        return paint.measureText(text);
    }

    private float getFontHeight(Paint paint, String text){
        Paint.FontMetrics fm = paint.getFontMetrics();
        return fm.ascent + fm.descent + fm.leading;
    }

}
