package com.yuge.yugecse.ext.annotation.view;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by bj on 2015/7/29.
 * 页面Fragment注解
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface FragmentInject {

    /** 布局文件id **/
    int value();

}
