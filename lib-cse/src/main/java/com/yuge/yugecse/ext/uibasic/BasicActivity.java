package com.yuge.yugecse.ext.uibasic;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.KeyEvent;
import com.yuge.yugecse.data.event.ActivityFinishEvent;
import com.yuge.yugecse.ext.annotation.event.BackActionEvent;
import com.yuge.yugecse.util.generic.ActivityUtil;

import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;


/**
 * Created by bj on 2015/7/29.
 * Basic of Activity , Every Activity should extend the class
 */
public class BasicActivity extends AppCompatActivity {

    /** Activity对象 **/
    protected Activity me;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setStatusBarColor(Color.parseColor("#ff303030"));
        EventBus.getDefault().register(this);
        this.me = this;//引用me自己，便于子类调用
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_BACK) {
            BackActionEvent backEvent = getClass().getAnnotation(BackActionEvent.class);
            if (backEvent != null && backEvent.isAllowDeal()) {
                this.me.finish();//关闭当前页面
                overridePendingTransition(backEvent.enterAnimId(), backEvent.exitAnimId());//使用动画操作
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * 处理页面关闭事件消息 - [EventBus]
     **/
    @Subscriber
    public void onFinishActivity(ActivityFinishEvent event) {
        ActivityUtil.goBack(this);
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

}
