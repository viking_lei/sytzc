package com.yuge.yugecse.ext.adapter;

import androidx.viewpager.widget.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by bj on 2015/8/6.
 * 基础页面数据适配器
 */
public class BasicPagerAdapter extends PagerAdapter {

    private List<View> viewContainer;

    public BasicPagerAdapter(){
        this(null);
    }

    public BasicPagerAdapter(List<View> views){
        if(this.viewContainer == null)
            this.viewContainer = new ArrayList<>();
        if(views!=null && views.size()>0) {
            this.viewContainer.addAll(views);
            notifyDataSetChanged();
        }
    }

    public void addViews(boolean isReset,View... views){
        if(isReset) this.viewContainer.clear();
        if(views!=null && views.length>0)
            this.viewContainer.addAll(Arrays.asList(views));
        notifyDataSetChanged();
    }

    public void removeView(View view){
        if(view!=null && viewContainer.contains(view))
            this.viewContainer.remove(view);
        notifyDataSetChanged();
    }

    public void removeViewAt(int position){
        if(position >= 0 && position <= this.viewContainer.size()){
            removeView(this.viewContainer.get(position));
        }
        notifyDataSetChanged();
    }

    public void clearViews(){
        this.viewContainer.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return this.viewContainer.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = viewContainer.get(position);
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(viewContainer.get(position));
    }

}
