package com.yuge.yugecse.ext.adapter;

import android.content.Context;
import androidx.annotation.Nullable;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by bj on 2015/7/29.
 * 数据列表适配器基类
 */
public abstract class BasicAdapter<T> extends BaseAdapter {

    protected Context context;
    private LayoutInflater inflater;
    protected List<T> list;
    private int layoutId;

    /** 构造函数 **/
    public BasicAdapter(@Nullable Context context,int layoutId){
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.layoutId = layoutId;
        this.list = new ArrayList<>();
    }

    /** 构造函数 **/
    public BasicAdapter(@Nullable Context context,int layoutId,@Nullable List<T> list){
        this.context = context;
        this.inflater = LayoutInflater.from(this.context);
        this.layoutId = layoutId;
        this.list = list;
    }

    /** 获取布局文件ID **/
    public int getLayoutId(){
        return this.layoutId;
    }

    /** 获取适配器数据 **/
    public List<T> getAdapterData(){
        return list;
    }

    /** 添加多条数据 **/
    public synchronized void addItems(T... items){
        if(items!=null && items.length > 0)
            list.addAll(Arrays.asList(items));
    }

    /** 添加多条数据 **/
    public synchronized void addItems(List<T> items){
        if(items!=null && items.size() > 0)
            list.addAll(items);
    }

    /** 删除某一项的数据 **/
    public synchronized void removeAt(int position){
        if(list!=null && list.size()>0 && position >0 && position < list.size())
            list.remove(position);
    }

    /** 删除指定的某一项数据 **/
    public synchronized void remove(@Nullable T item){
        if(list!=null && list.size()>0){
            int index = -1;
            for(int i=0;i<list.size();i++){
                if(item == list.get(i))
                    index = i;
            }
            if(index!=-1) removeAt(index);
        }
    }

    /**
     * 设置某一项的数据
     * @param position
     * @param item
     */
    public synchronized void setItem(int position,T item){
        if(list!=null && list.size()>0 && position >0 && position < list.size())
            list.set(position,item);
    }

    /** 清空数据源 **/
    public synchronized void clear(){
        if(list!=null && list.size()>0)
            list.clear();
    }

    /** 刷新数据 **/
    public void refresh(){
     this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return list == null?null:list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null){
            convertView = inflater.inflate(this.layoutId,null);
            holder = new ViewHolder(convertView);
            this.initItemView(holder);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder)convertView.getTag();
        }
        this.setItemValue(holder,position,list.get(position));
        return convertView;
    }

    /** 初始化列表项的View **/
    public abstract void initItemView(ViewHolder holder);

    /** 设置列表项的值 **/
    public abstract void setItemValue(ViewHolder holder,int position,T item);

    public final class ViewHolder{

        private SparseArray<View> viewStock;
        private View itemView;

        public ViewHolder(View itemView){
            this.itemView = itemView;
            viewStock = new SparseArray<>();
        }

        /** 添加子类ID **/
        public void addChildIds(int... ids){
            if(ids!=null && ids.length > 0){
                for(int i=0;i<ids.length;i++)
                    viewStock.put(ids[i],this.itemView.findViewById(ids[i]));
            }
        }

        /** 根据子ViewId获取View对象 **/
        public View getChildById(int id){
            return viewStock.get(id);
        }

    }

}
