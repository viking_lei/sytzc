package com.yuge.yugecse.ext.annotation.event;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by bj on 2015/7/29.
 * 返回事件注解
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface BackActionEvent {

    /** 是否允许处理返回 **/
    boolean isAllowDeal() default true;

    /** 进入时的动画,默认为从右边进入 **/
    int enterAnimId() default android.R.anim.slide_in_left;

    /** 退出时的动画，默认从左边出去 **/
    int exitAnimId() default android.R.anim.slide_out_right;

}
