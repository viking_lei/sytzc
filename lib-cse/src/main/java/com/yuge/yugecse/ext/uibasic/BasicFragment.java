package com.yuge.yugecse.ext.uibasic;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yuge.yugecse.ext.annotation.view.FragmentInject;

import org.simple.eventbus.EventBus;

/**
 * Created by bj on 2015/7/29.
 * BasicFragment class ,all of fragment should extend it
 */
public class BasicFragment extends Fragment {

    public static String TAG = BasicFragment.class.getSimpleName();
    public Activity context;//Activity对象

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = (Activity) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FragmentInject fragmentInject = getClass().getAnnotation(FragmentInject.class);
        if (fragmentInject != null) {
            View rootView = inflater.inflate(fragmentInject.value(), null);
            EventBus.getDefault().register(this);//注册EventBus
            this.initView();//初始化View的数据
            return rootView;
        }
        Log.e(TAG, "子类实现没有添加类的注解 - FragmentInject");
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    /**
     * 初始化View的数据
     **/
    public void initView() {
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

}
