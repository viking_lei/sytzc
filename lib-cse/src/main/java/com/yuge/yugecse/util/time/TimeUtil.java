package com.yuge.yugecse.util.time;

import android.annotation.SuppressLint;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by bj on 2015/8/11.
 * 时间辅助函数
 */
public class TimeUtil {

    /**
     * 比较当前时间，得出时间描述
     **/
    public static String diffWidthCurrent(long time) {
        long diffVal = System.currentTimeMillis() - time;
        if (diffVal < 1 * 60 * 1000) {
            return "刚刚";
        }
        if (diffVal < 1 * 60 * 60 * 1000) {
            return (int) (diffVal / (1 * 60 * 1000)) + "分钟前";
        }
        if (diffVal < 1 * 24 * 60 * 60 * 1000) {
            return (int) (diffVal / (1 * 60 * 60 * 1000)) + "小时前";
        }
        if (diffVal < 30 * 24 * 60 * 60 * 1000) {
            return (int) (diffVal / (1 * 24 * 60 * 60 * 1000)) + "天前";
        }
        return format(long2date(time), "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * long型转换为日期类型
     **/
    public static Date long2date(long time) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        return calendar.getTime();
    }

    /**
     * 格式化日期
     **/
    @SuppressLint("SimpleDateFormat")
    public static String format(Date date, String format) {
        return new SimpleDateFormat(format).format(date);
    }

    /**
     * 获取今天凌晨0点
     **/
    public static long getTodayMorningMillins() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.AM_PM, Calendar.AM);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        Log.i("今早凌晨00:00", calendar.toString());
        return calendar.getTime().getTime();
    }

    /**
     * 获取今天深夜最后1毫秒
     **/
    public static long getTodayDeepNightMillins() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.AM_PM, Calendar.PM);
        calendar.set(Calendar.HOUR, 11);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        Log.i("今晚深夜11:59", calendar.toString());
        return calendar.getTime().getTime();
    }

}
