package com.yuge.yugecse.util.device;

import android.content.Context;
import androidx.annotation.StringRes;
import android.widget.Toast;

/**
 * Created by Administrator on 2015/3/12.
 * 提示框辅助类
 */
public class ToastUtil {

    /** 顯示提示信息 **/
    public static void showGenericToast(Context context,@StringRes int messageId){
        showGenericToast(context,context.getResources().getString(messageId));
    }

    /**
     * 显示默认的提示框
     * @param context
     * @param message
     */
    public static void showGenericToast(Context context,String message){
        Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
//        View rootView = View.inflate(context, R.layout.ext_toast_message,null);
//        Toast toast = new Toast(context);
//        toast.setView(rootView);
//        ((TextView)rootView.findViewById(R.id.txtMessage)).setText(message);
//        toast.show();
    }

    /**
     * 显示消息提示
     * @param context
     * @param message
     * @param gravity
     * @param during
     */
    public static void showToast(Context context,String message,int gravity,int during){
//        View rootView = View.inflate(context, R.layout.ext_toast_message, null);
        Toast toast = new Toast(context);
        toast.setText(message);
        toast.setDuration(during);
//        toast.setView(rootView);
//        ((TextView)rootView.findViewById(R.id.txtMessage)).setText(message);
        toast.setGravity(gravity,0,0);
        toast.show();
    }

    /**
     * 显示消息提示
     * @param context
     * @param message
     * @param gravity
     */
    public static void showLongToast(Context context,String message,int gravity){
        showToast(context,message,gravity,Toast.LENGTH_LONG);
    }

    /**
     * 显示消息提示
     * @param context
     * @param message
     * @param gravity
     */
    public static void showShortToast(Context context,String message,int gravity){
        showToast(context,message,gravity,Toast.LENGTH_SHORT);
    }

}