package com.yuge.yugecse.util.config;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by Administrator on 2015/3/20.
 * 配置文件辅助类
 */
public class ConfigUtil {

    private SharedPreferences config;
    private SharedPreferences.Editor editor;

    private static ConfigUtil configUtils;

    public static ConfigUtil getInstance(Context context,String filename){
        if(configUtils == null) {
            configUtils = new ConfigUtil(context,filename);
        }
        return configUtils;
    }

    private ConfigUtil(Context context, String filename){
        config = context.getSharedPreferences(filename,Context.MODE_PRIVATE);
        editor = config.edit();
    }

    /**
     * 写入数据
     * @param keyname
     * @param data
     */
    @SuppressWarnings("all")
    public void put(String keyname,Object data){
        if(data instanceof Integer){
            editor.putInt(keyname, (Integer) data);
        }else if(data instanceof Long){
            editor.putLong(keyname, (Long) data);
        }else if(data instanceof Boolean){
            editor.putBoolean(keyname, (Boolean) data);
        }else if(data instanceof Float){
            editor.putFloat(keyname, (Float) data);
        }else if(data instanceof HashSet<?>){
            editor.putStringSet(keyname,(Set<String>)data);
        }else{
            editor.putString(keyname,(String)data);
        }
        try {
            editor.apply();
        }catch (Exception e){
            e.printStackTrace();
            editor.commit();
        }
    }

    /**
     * 读取数据
     * @param keyname
     * @param defaultValue
     * @return
     */
    @SuppressWarnings("all")
    public Object get(String keyname,Object defaultValue){
        if(defaultValue instanceof Integer){
            return config.getInt(keyname, (Integer) defaultValue);
        }else if(defaultValue instanceof Long){
            return config.getLong(keyname, (Long) defaultValue);
        }else if(defaultValue instanceof Boolean){
            return config.getBoolean(keyname, (Boolean) defaultValue);
        }else if(defaultValue instanceof Float){
            return config.getFloat(keyname, (Float) defaultValue);
        }else if(defaultValue instanceof HashSet<?>){
            return config.getStringSet(keyname,(HashSet<String>)defaultValue);
        }else{
            return config.getString(keyname,(String)defaultValue);
        }
    }

    /**
     * 是否包某个值
     * @param keyname
     * @return
     */
    public boolean contains(String keyname){
        return config.contains(keyname);
    }

    /**
     * 删除某个键值
     * @param keyname
     */
    public void remove(String keyname){
        if(contains(keyname)){
            try {
                editor.remove(keyname);
                editor.apply();
            }catch (Exception e){
                e.printStackTrace();
                editor.commit();
            }
        }
    }

    /**
     * 删除key
     * @param keyname
     */
    public void removeKey(String keyname){
        try{
            editor.remove(keyname);
            editor.apply();
        }catch (Exception e){
            e.printStackTrace();
            editor.commit();
        }
    }

    /**
     * 清空所有数据
     */
    public void clear(){
        editor.clear();
    }

    /**
     * 获得所有数据
     * @return
     */
    public Map<String,?> getAll(){
        return config.getAll();
    }

}