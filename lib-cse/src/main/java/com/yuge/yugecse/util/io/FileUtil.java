package com.yuge.yugecse.util.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by bj on 2015/7/29.
 * 文件处理工具类
 */
public class FileUtil {


    /** 获取文件夹大小 **/
    public static long getDirectorySize(final String directory){
        long length = 0;
        File file = new File(directory);
        if(file.isDirectory()){
            for(File f : file.listFiles())
                length += f.length();
        }else if(file.isFile()){
            length = file.length();
        }
        return length;
    }

    /**
     * 写入文件到本地
     * @param filename 文件名称
     * @param data 数据内容
     * @throws Exception
     */
    public static void write2file(String filename,byte[] data) throws Exception{
        File file = new File(filename);
        if(file.getParentFile().isDirectory() || file.getParentFile().mkdirs()){
            if(file.exists()) file.delete();//如果文件已经存在，则删除该文件
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(data,0,data.length);
            fos.close();
        }else{
            throw new Exception("写入文件异常");
        }
    }

    /**
     * 写入数据到本地文件
     * @param filename 文件名称
     * @param content 要写入的内容
     * @throws Exception
     */
    public static void write2file(String filename,String content) throws Exception{
        File file = new File(filename);
        if(file.getParentFile().isDirectory() || file.getParentFile().mkdirs()){
            if(file.exists()) file.delete();//如果文件已经存在，则删除该文件
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(filename.getBytes("utf-8"));
            fos.close();
        }else{
            throw new Exception("写入文件异常");
        }
    }

    /**
     * 写入数据到本地文件
     * @param filename 文件名称
     * @param stream 要写入的数据流
     * @throws Exception
     */
    public static void write2file(String filename,InputStream stream) throws Exception{
        File file = new File(filename);
        if(file.getParentFile().isDirectory() || file.getParentFile().mkdirs()){
            if(file.exists()) file.delete();//如果文件已经存在，则删除该文件
            FileOutputStream fos = new FileOutputStream(file);
            byte[] buffer = new byte[2048];
            int length = 0;//每次取得的数据长度，为-1则表示已经读取完整
            while((length = stream.read(buffer))!=-1)
                fos.write(buffer,0,length);
            stream.close();
            fos.close();
        }else{
            throw new Exception("写入文件异常");
        }
    }

    /** 数据流读取为字符串 **/
    public static String stream2string(InputStream stream) throws Exception{
        if(stream == null) return "";
        String lineContent = null;
        StringBuilder strBuilder = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        while((lineContent = reader.readLine())!=null)
            strBuilder.append(lineContent);
        reader.close();
        return strBuilder.toString();
    }

}
