package com.yuge.yugecse.data.event;

/**
 * Created by bj on 2015/7/29.
 * 页面关闭事件类，用于EventBus处理消息
 */
public class ActivityFinishEvent {}
