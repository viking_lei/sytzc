package com.yuge.yugecse.data.network;

/**
 * Created by bj on 2015/8/18.
 * 网络信息接口类
 */
public interface NetworkInterface {

    /**  获取连接超时时长  **/
    int getConnectTimeout();

    /**  获取读取超时时长 **/
    int getReadTimeout();

}
